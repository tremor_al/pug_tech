// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#include "pug_tech_2Character.h"
#include "Kismet/HeadMountedDisplayFunctionLibrary.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/Controller.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/SpringArmComponent.h"

//////////////////////////////////////////////////////////////////////////
// Apug_tech_2Character

Apug_tech_2Character::Apug_tech_2Character()
{
	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// set our turn rates for input
	BaseTurnRate = 45.f;
	BaseLookUpRate = 45.f;

	// Don't rotate when the controller rotates. Let that just affect the camera.
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Character moves in the direction of input...	
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 540.0f, 0.0f); // ...at this rotation rate
	GetCharacterMovement()->JumpZVelocity = 600.f;
	GetCharacterMovement()->AirControl = 0.2f;

	// Create a camera boom (pulls in towards the player if there is a collision)
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->TargetArmLength = 300.0f; // The camera follows at this distance behind the character	
	CameraBoom->bUsePawnControlRotation = true; // Rotate the arm based on the controller

	// Create a follow camera
	FollowCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FollowCamera"));
	FollowCamera->SetupAttachment(CameraBoom, USpringArmComponent::SocketName); // Attach the camera to the end of the boom and let the boom adjust to match the controller orientation
	FollowCamera->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	// Note: The skeletal mesh and anim blueprint references on the Mesh component (inherited from Character) 
	// are set in the derived blueprint asset named MyCharacter (to avoid direct content references in C++)
}

//////////////////////////////////////////////////////////////////////////
//
void Apug_tech_2Character::BeginPlay()
{
	Super::BeginPlay();
	PlayerController = Cast<APlayerController>(Controller);
}
// Called every frame
void Apug_tech_2Character::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	MoveForward(VelocityMovement);
	//SwipeLeft();
	if (ScreenPressed) 
	{
		FVector2D TouchCurrent2DVector;
		bool h;
		PlayerController->GetInputTouchState(ETouchIndex::Touch1, TouchCurrent2DVector.X, TouchCurrent2DVector.Y, h);
		float s = FMath::Sqrt(TouchStart2DVector.X*TouchCurrent2DVector.X + TouchStart2DVector.Y*TouchCurrent2DVector.Y);
		if (s > 100.0f)
		{
			ScreenPressed = false;
		}
	}
	if (strife_right == true)
	{
		Strafe(VelocityMovement);
	}
	if (strife_left == true)
	{
		Strafe(-1*VelocityMovement);
	}
}

// Input
void Apug_tech_2Character::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	// Set up gameplay key bindings
	check(PlayerInputComponent);
	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);

	PlayerInputComponent->BindAxis("MoveForward", this, &Apug_tech_2Character::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &Apug_tech_2Character::MoveRight);

	// We have 2 versions of the rotation bindings to handle different kinds of devices differently
	// "turn" handles devices that provide an absolute delta, such as a mouse.
	// "turnrate" is for devices that we choose to treat as a rate of change, such as an analog joystick
	PlayerInputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis("TurnRate", this, &Apug_tech_2Character::TurnAtRate);
	PlayerInputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);
	PlayerInputComponent->BindAxis("LookUpRate", this, &Apug_tech_2Character::LookUpAtRate);

	// handle touch devices
	PlayerInputComponent->BindTouch(IE_Pressed, this, &Apug_tech_2Character::TouchStarted);
	PlayerInputComponent->BindTouch(IE_Released, this, &Apug_tech_2Character::TouchStopped);

	// VR headset functionality
	PlayerInputComponent->BindAction("ResetVR", IE_Pressed, this, &Apug_tech_2Character::OnResetVR);
}

void Apug_tech_2Character::OnResetVR()
{
	UHeadMountedDisplayFunctionLibrary::ResetOrientationAndPosition();
}

void Apug_tech_2Character::TouchStarted(ETouchIndex::Type FingerIndex, FVector Location)
{
		//Jump();
		bool h;
		PlayerController->GetInputTouchState(FingerIndex, TouchStart2DVector.X, TouchStart2DVector.Y, h);
		ScreenPressed = true;
		//UE_LOG(LogScript, Warning, TEXT("ScreenLoc Dep %f %f Pressed %f"), TouchStart2DVector.X, TouchStart2DVector.Y, ScreenPressed);
}

void Apug_tech_2Character::TouchStopped(ETouchIndex::Type FingerIndex, FVector Location)
{
		//StopJumping();
		ScreenPressed = false;

		bool h;
		PlayerController->GetInputTouchState(FingerIndex, TouchEnd2DVector.X, TouchEnd2DVector.Y, h);
		FVector2D lengthAbs (FMath::Abs(TouchStart2DVector.X - TouchEnd2DVector.X), FMath::Abs(TouchStart2DVector.Y - TouchEnd2DVector.Y));
		FVector2D length(TouchStart2DVector.X - TouchEnd2DVector.X, TouchStart2DVector.Y - TouchEnd2DVector.Y);
		if (lengthAbs.X > lengthAbs.Y)
		{
			//left right
			if (length.X > 0)
			{
				UE_LOG(LogScript, Warning, TEXT("LEFT"));
				//Strafe(1.0);
				strife_left = true;
				start = FPlatformTime::Seconds();
				
			}
			else
			{
				UE_LOG(LogScript, Warning, TEXT("RIGHT"));
				strife_right = true;
				start = FPlatformTime::Seconds();
				//Strafe(-1.0);
			}
			
		}
		else 
		{
			//up down
			if (length.Y > 0)
			{
				UE_LOG(LogScript, Warning, TEXT("UP"));
			}
			else
			{
				UE_LOG(LogScript, Warning, TEXT("DOWN"));
			}
		}
}

void Apug_tech_2Character::Strafe(float Value)
{
	float LastIndex =0;
	float NumObjectsPerTick = 500;
	for (int i = 0; i < LastIndex + NumObjectsPerTick; i++)
	{
		// find out which way is right
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// get right vector 
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);
		// add movement in that direction
		//AddMovementInput(FVector(0.0f, 0.0f, 1.0f), 1.0);
		//UE_LOG(LogScript, Warning, TEXT("straffing"));
		if (i == (LastIndex + NumObjectsPerTick))
		{
			LastIndex = i;
		}
	}
	end = FPlatformTime::Seconds();
	if ((end - start) >= 0.5f) {
		strife_right = false;
		strife_left = false;
	}
	//UE_LOG(LogTemp, Warning, TEXT("code executed in %f seconds."), end - start);
	AddMovementInput(FVector(0.0f, 1.0f, 0.0f), Value);
		
}

void Apug_tech_2Character::TurnAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerYawInput(Rate * BaseTurnRate * GetWorld()->GetDeltaSeconds());
}

void Apug_tech_2Character::LookUpAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerPitchInput(Rate * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
}

void Apug_tech_2Character::MoveForward(float Value)
{
	if ((Controller != NULL) && (Value != 0.0f))
	{
		// find out which way is forward
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// get forward vector
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
		//Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
		//UE_LOG(LogTemp, Warning, TEXT("COSO %s"), *Direction.ToString());
		//UE_LOG(LogTemp, Warning, TEXT("COSO %s"), Value);
		//AddMovementInput(Direction, Value);
		AddMovementInput(FVector(1.0f, 0.0f, 0.0f), Value);
	}
}

void Apug_tech_2Character::MoveRight(float Value)
{
	if ( (Controller != NULL) && (Value != 0.0f) )
	{
		// find out which way is right
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);
	
		// get right vector 
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);
		// add movement in that direction
		AddMovementInput(Direction, Value);
	}
}

void Apug_tech_2Character::SwipeLeft() 
{
	//FVector2D TouchStart = PlayerController->GetInputTouchState();
	
	//const FVector2D TouchStart = APlayerController(GetInputTouchState).
	//APlayerController PlayerController = Controller->CastToPlayerController;
	//FVector2D TouchStart = *Controller->CastToPlayerController->GetInputTouchState();
	//UE_LOG(LogTemp, Warning, TEXT("COSO %s"), *TouchStart.ToString());
	//const FVector2D TouchStart =FVector2D(PlayerController)
	// PlayerController->GetInputTouchState();
	//const FVector2D TouchStart = playercontroller
}
